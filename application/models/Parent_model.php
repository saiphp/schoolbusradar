<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parent_model extends CI_Model {

	public function saveParent($data)
	{
		if ($this->db->insert('parents', $data)) {
			return TRUE;
		}

		return FALSE;
	}

	public function getAllParents()
	{
		$res = $this->db->get('parents');

		if ($res->num_rows() > 0) {
			return $res->result();
		}
		return FALSE;

	}
}