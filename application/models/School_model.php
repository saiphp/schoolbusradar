<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class School_model extends CI_Model

{

	public function getAll()

	{

		$schools = $this->db->get('schools');

		if ($schools->num_rows() > 0) {

			return $schools->result();

		}

	}



	public function getAllUsers()

	{

		$users = $this->db->get('users');

		if ($users->num_rows() > 0) {

			return $users->result();

		}

	}



	public function getAllInquiries()

	{

		$users = $this->db->get('inquiries');

		if ($users->num_rows() > 0) {

			return $users->result();

		}

	}

	public function getSchoolById($id)
	{
		$this->db->where('id', $id);
		$res = $this->db->get('schools');
		if ($res->num_rows() > 0) {
			foreach ($res->result() as $r) {
				return $r->name;
			}
		}
		return FALSE;
	}



	public function saveUser($data)

	{

		if ($this->db->insert('users', $data)) {

			return TRUE;

		 }

		return FALSE;

	}



	public function saveSchool($data)

	{

		if ($this->db->insert('schools', $data)) {

			return TRUE;

		 }

		return FALSE;

	}



	public function saveInquiry($data)

	{

		if ($this->db->insert('inquiries', $data)) {

			return TRUE;

		}

		return FALSE;

	}


}