<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class SchoolController extends MY_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->model('school_model', 'school');

		$this->load->library(array('ion_auth','form_validation'));

	}



	public function index()

	{

		echo json_encode($this->school->getAll());

	}



	public function users()

	{

		echo json_encode($this->school->getAllUsers());

	}



	public function inquiry()

	{

		echo json_encode($this->school->getAllInquiries());

	}



	public function saveUsers()
	{

		$tables = $this->config->item('tables','ion_auth');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}



		$tables = $this->config->item('tables','ion_auth');

		$identity_column = $this->config->item('identity','ion_auth');

		$this->form_validation->set_rules('username', 'Username', 'required|is_unique['.$tables['users'].'.username]');

		$this->form_validation->set_rules('first_name', 'First Name', 'required');

		$this->form_validation->set_rules('last_name', 'Last Name', 'required');

		$this->form_validation->set_rules('company', 'Company', 'required');

		$this->form_validation->set_rules('phone', 'Phone', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique['.$tables['users'].'.email]');

		// $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');

		// $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|trim');

		$this->form_validation->set_rules('schools', 'School', 'required|callback_check_default');

		$this->form_validation->set_message('check_default', 'You need to select school group');



		$response = [];

		

		if ($this->form_validation->run() == TRUE)
		{

			$this->load->library('email');

			$email    = strtolower($this->input->post('email'));

			$identity = ($identity_column==='email') ? $email : $this->input->post('username');

            $password = $this->randomPassword();

            $this->email->from('besabellacyrus@gmail.com', 'School Bus Service Inc.');
			$this->email->to($email);

			$this->email->subject('School Bus Service');
			$this->email->message('Your Username '. $this->input->post('username').' Your temporary password ' . $password);

			if (!$this->email->send()) {
				$response['mail_status'] = 400;
				$response['mail_message'] = 'Error not sending email.';
			} else {
				$response['mail_status'] = 200;
				$response['mail_message'] = 'Sent successfully.';
			}
			

			$additional_data = array(

				'username' 	=> $this->input->post('username'),

				'first_name' 	=> $this->input->post('first_name'),

				'last_name' 	=> $this->input->post('last_name'),

				'company' 		=> $this->input->post('company'),

				'phone' 		=> $this->input->post('phone'),

				'school'		=> $this->input->post('schools'),

				);



			if($this->form_validation->run() == TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data))

			{

				$response['status'] = 200;

				$response['message'] = 'Successfully added new user!';

			} else {

				$response['status'] = 400;

				$response['error'] = 'Something went wrong!';

			}

		} else {

			$response['status'] = 400;

			$response['error'] = validation_errors();

		}

		echo json_encode($response);

	}


	public function randomPassword()
	{
		$this->load->helper('string');

		return random_string('alnum', 7);
	}



	function check_default($post_string)

	{

		return $post_string == '0' ? FALSE : TRUE;

	}



	public function saveSchool()

	{

		$this->form_validation->set_rules('group_name', 'School Name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		$response = [];



		if ( $this->form_validation->run() == TRUE ) {

			$data = array(

				'name' => $this->input->post('group_name'),
				'address' => $this->input->post('address'),
				'description' => $this->input->post('description'),

				);



			if($this->school->saveSchool($data))

			{

				$response['status'] = 200;

				$response['message'] = 'Successfully added new School!';

			} else {

				$response['status'] = 400;

				$response['error'] = 'Something went wrong!';

			}

		} else {

			$response['status'] = 400;

			$response['error'] = validation_errors();

		}



		echo json_encode($response);

		

	}

	public function addStudent()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('login', 'refresh');
		}
		
		$this->school_template('schools/add_student_view');
	}

	public function addParent()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('login', 'refresh');
		}
		
		$this->school_template('schools/add_parent_view');
	}

	public function saveStudent()
	{

		
	}

	public function saveParent()
	{
		$this->load->model('parent_model', 'parent');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');

		if ( $this->form_validation->run() == TRUE ) {
			
			$data = array(
				'username'      => $this->input->post('username'),
				'first_name'    => $this->input->post('first_name'),
				'last_name'     => $this->input->post('last_name'),
				'address'       => $this->input->post('address'),
				'mobile_number' => $this->input->post('mobile_number'),
			);
			if($this->parent->saveParent($data))
			{

				$response['status'] = 200;

				$response['message'] = 'Successfully added new Parent!';

			} else {

				$response['status'] = 400;

				$response['error'] = 'Something went wrong!';

			}
		} else {
			$response['status'] = 400;

			$response['error'] = validation_errors();
		}

		echo json_encode($response);

	}

	public function getAllStudent()
	{
		$this->school->getAllStudent();
	}



}