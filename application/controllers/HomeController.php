<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class HomeController extends MY_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->library('form_validation');

		$this->load->model('school_model', 'school');
		

	}



	public function index()

	{

		$this->site_template('home_view');

	}



	public function portal()

	{

		echo 'Portal';

	}



	public function about()

	{

		$this->site_template('about_view');

	}



	public function contact()

	{

		$this->site_template('contact_view');

	}



	public function login()

	{

		$this->site_template('login_view');

	}



	public function submitContact()

	{

		$this->load->library('form_validation');



		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

		$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim|required');

		$this->form_validation->set_rules('message', 'Message', 'trim|required');

		$response = [];



		if ($this->form_validation->run() == TRUE)

        {

        	$data = array(

        		'name' 				=> $this->input->post('name'),

        		'email' 			=> $this->input->post('email'),

        		'contact_number' 	=> $this->input->post('contact_number'),

        		'message' 			=> $this->input->post('message'),

        		);



        	if ($this->school->saveInquiry($data) == FALSE) {

        		$response['error'] = 'Something went wrong.';

        	} else {

        		$response['message'] = 'Thank you for your inquiries we will reply to you shortly';

        	}



        } else {

        	$response['error'] = validation_errors();

        }



        echo json_encode($response);

	}



}