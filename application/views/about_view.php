<div class="container">
	<div class="row about_us">
		<div class="col-md-12">
			<h1>ABOUT US</h1>
			<hr>
			<p>
				The Student Tracking System for School Bus is a system that is created for the Lavides School Bus Company. It is an added service for parents and students who will avail or who is currently availing for the company’s services. 
			</p>
			<h3>RFID  TECHNOLOGY</h3>
			<p>
				The RFID Technology will be the main feature of the system. This technology is the heart that triggers the flow of each and every module included in the system. Each student of the school bus will be given an RFID card that they will use in boarding and departing from the school bus.   
			</p>
			<h3>SMS NOFITICATION</h3>
			<p>
				The system’s SMS Notification module will be responsible for sending SMS messages for each and every parent. Upon tapping of RFID Card, an SMS message is sent to the child’s parent notifying them the exact time of boarding and departing of their child from the school bus.
			</p>
			<h3>PARENT & SCHOOL WEB PORTAL</h3>
			<p>
				Each and every parent that avails for the school bus service of Lavides School Bus Company is given an account. This account given to the school and parent is then used to view the Timetable history of their Child/Students.
			</p>
		</div>
	</div>
</div>