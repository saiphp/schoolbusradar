<style>
	#map {
	  width:100%;
	  height:450px;
	}
</style>
<div class="container dashboard">
	<h1>Admin Dashboard</h1>
	<hr>
	<div class="col-md-3">
		<div class="side_nav">
			<ul class="text-center nav" role="tablist">
				<li class="active"><a href="#list_school" aria-controls="list_school" role="tab" data-toggle="tab">SCHOOLS</a></li>
				<li><a href="#users_list" aria-controls="users" role="tab" data-toggle="tab">USERS</a></li>
				<li><a href="#add_school" id="mapTab" data-toggle="tab">ADD SCHOOLS</a></li>
				<li><a href="#add_user" aria-controls="add_user" role="tab" data-toggle="tab">ADD USERS</a></li>
				<li><a href="#inquiry" aria-controls="inquiry" role="tab" data-toggle="tab">INQUIRIES</a></li>
			</ul>
		</div>
	</div>
	<div class="col-md-9">
		<?php echo isset($message) ? $message : ''; ?>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="list_school">
				<table id="school_table" class="display" >
					<thead>
						<tr>
							<th>id</th>
							<th>name</th>
							<th>description</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>id</td>
							<td>name</td>
							<td>description</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="users_list">
				<table id="user_table" class="display" style="width:100%;">
					<thead>
						<tr>
							<th>ID</th>
							<th>username</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Company</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="add_school">
				<div class="col-md-6">
					<?php echo form_open("auth/create_group", array('id' => 'schoolForm'));?>
					<div class="form-group">
						<label>School Name</label>
						<input type="text" name="group_name" class="form-control">
					</div>
					<div class="form-group">
						<label>Address</label>
						<input id="pac-input" name="address" class="form-control" type="text" placeholder="Search Box">
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" name="description"></textarea>
					</div>
					<div class="form-group">
						<?php echo form_submit('submit', 'SAVE', array('class' => 'form-control app-btn', 'id' => 'school_submit'));?>
					</div>
					<?php echo form_close();?>
				</div>
				<div class="col-md-6">
				
					<div id="map"></div>
					<script>
					      // This example adds a search box to a map, using the Google Place Autocomplete
					      // feature. People can enter geographical searches. The search box will return a
					      // pick list containing a mix of places and predicted search terms.

					      // This example requires the Places library. Include the libraries=places
					      // parameter when you first load the API. For example:
					      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

					      function initAutocomplete() {
					        map = new google.maps.Map(document.getElementById('map'), {
					          center: {lat: 14.5547, lng: 121.0244},
					          zoom: 13,
					          mapTypeId: 'roadmap'
					        });

					        // Create the search box and link it to the UI element.
					        var input = document.getElementById('pac-input');
					        var searchBox = new google.maps.places.SearchBox(input);
					        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

					        // Bias the SearchBox results towards current map's viewport.
					        map.addListener('bounds_changed', function() {
					          searchBox.setBounds(map.getBounds());
					        });

					        var markers = [];
					        // Listen for the event fired when the user selects a prediction and retrieve
					        // more details for that place.
					        searchBox.addListener('places_changed', function() {
					          var places = searchBox.getPlaces();

					          if (places.length == 0) {
					            return;
					          }

					          // Clear out the old markers.
					          markers.forEach(function(marker) {
					            marker.setMap(null);
					          });
					          markers = [];

					          // For each place, get the icon, name and location.
					          var bounds = new google.maps.LatLngBounds();
					          places.forEach(function(place) {
					            if (!place.geometry) {
					              console.log("Returned place contains no geometry");
					              return;
					            }
					            var icon = {
					              url: place.icon,
					              size: new google.maps.Size(71, 71),
					              origin: new google.maps.Point(0, 0),
					              anchor: new google.maps.Point(17, 34),
					              scaledSize: new google.maps.Size(25, 25)
					            };

					            // Create a marker for each place.
					            markers.push(new google.maps.Marker({
					              map: map,
					              icon: icon,
					              title: place.name,
					              position: place.geometry.location
					            }));

					            if (place.geometry.viewport) {
					              // Only geocodes have viewport.
					              bounds.union(place.geometry.viewport);
					            } else {
					              bounds.extend(place.geometry.location);
					            }
					          });
					          map.fitBounds(bounds);
					        });
					      }
					</script>
					
					
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="add_user">
				<?php echo form_open("SchoolController/saveUsers", array('id' => 'userForm'));?>
				<div class="form-group">
					<label>Username</label>
					<input type="text" class="form-control" name="username">
				</div>
				<div class="form-group">
					<label>First Name</label>
					<input type="text" class="form-control" name="first_name">
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input type="text" class="form-control" name="last_name">
				</div>
				<div class="form-group">
					<label>Company</label>
					<input type="text" class="form-control" name="company">
				</div>
				<div class="form-group">
					<label>Phone</label>
					<input type="text" class="form-control" name="phone">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="text" class="form-control" name="email">
				</div>
				<!-- <div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<label>Confirm Password</label>
					<input type="password" class="form-control" name="confirm_password">
				</div> -->
				<div class="form-group">
					<label>School</label>
					<select class="form-control" name="schools">
						<option value="0">Choose...</option>
						<?php foreach ($schools as $sch): ?>
							<option value="<?= $sch->id; ?>"><?= $sch->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<input id="user_submit" type="submit" class="form-control app-btn" value="SAVE">
				</div>
				<?php echo form_close();?>
			</div>
			<div role="tabpanel" class="tab-pane" id="inquiry">
				<table id="inquiry_table" class="display" >
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Contact Number</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>ID</td>
							<td>Name</td>
							<td>Email</td>
							<td>Contact Number</td>
							<td>Message</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div> <!-- /tab-content -->
	</div>
</div>

