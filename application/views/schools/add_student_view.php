<div class="container">
	<div class="row add_student">
		<div class="col-md-12">
		<h2>Add Student</h2>
		<div class="row">
			<div class="col-md-6">
				<form id="studentForm">
					<div class="form-group">
						<label>RFID no.</label>
						<input type="text" name="rfid" class="form-control">
					</div>
					<div class="form-group">
						<label>First Name</label>
						<input type="text" class="form-control" name="first_name">
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input type="text" class="form-control" name="last_name">
					</div>
					<div class="form-group">
						<label>Parent / Guardian</label>
						<select class="form-control" name="parent">
							<option value="0">Select Parent</option>
						</select>
					</div>
					<div class="form-group">
						<input type="submit" class="form-control app-btn" value="SAVE" name="">
					</div>
				</form>
			</div>
			<div class="col-md-6">
				
			</div>
		</div>
			
		</div>
	</div>
</div>
<script>
	$('#parent_submit').on('click', function(e){
		e.preventDefault();
		var user_data = $("#parentForm").serialize();
		var user_url = '<?= base_url(); ?>SchoolController/saveParent';

		$.ajax({
			type: "POST",
			url: user_url,
			dataType: 'json',
			data: user_data,
			success: function(response){

				if (response.status == 400) {
					smoke.signal(response.error, function(e){

					}, {
						duration: 2000,
						classname: ""
					});
				} else {
					smoke.signal(response.message, function(e){
					}, {
						duration: 2000,
						classname: ""
					});
					$('#parentForm')[0].reset();
				}
			}
		});
	});
</script>