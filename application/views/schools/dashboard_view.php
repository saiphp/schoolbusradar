<div class="container">

	<div class="col-md-12 school_dashboard">
		<h1>SCHOOL PORTAL</h1>

		<hr>

		<div class="row">

			<div class="col-md-4">

				<h3>NAME OF SCHOOL</h3>

				<p><?= $school_name; ?></p>

				<h3>LIST OF STUDENTS</h3>

				<ul class="nav" role="tablist">

					<li class="active"><a href="#list_school" aria-controls="list_school" role="tab" data-toggle="tab">1. Student Name</a></li>

				</ul>

			</div>

			<div class="col-md-8">

				<div class="tab-content">

					<div role="tabpanel" class="tab-pane active" id="list_school">

						 <div class="row">

						 	<div class="col-md-6">

						 		<h5>RFID NO. 9999999999999</h5>

						 		<h5>NAME:JUAN DELA CRUZ</h5>

						 		<h5>PARENT / GUARDIAN: JUANA DELA CRUZ</h5>

						 	</div>

						 	<div class="col-md-6">

						 		<h5>SCHOOL BUS NO: UVY 540</h5>

						 		<h5>DRIVER'S NAME: JOHN DOE</h5>

						 		<h5>CONTACT NUMBER: +6300000000</h5>

						 	</div>

						 </div>

						 <div class="row">

						 	DATA HERE

						 </div>

					</div>

					</div>

			</div>

		</div>

	</div>

</div>