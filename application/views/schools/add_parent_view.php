<div class="container">
	<div class="row add_parent">
		<div class="col-md-12">
			<h3>Add Parent</h3>
			<hr>
			<div class="col-md-6">
			<form id="parentForm">
				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control">
				</div>
				<div class="form-group">
					<label>First Name</label>
					<input type="text" name="first_name" class="form-control">
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input type="text" name="last_name" class="form-control">
				</div>
				<div class="form-group">
					<label>Address</label>
					<input type="text" name="address" class="form-control">
				</div>
				<div class="form-group">
					<label>Mobile Number</label>
					<input type="text" name="mobile_number" class="form-control">
				</div>
				<div class="form-group">
					<input type="submit" id="parent_submit" class="form-control app-btn" value="SAVE" name="">
				</div>
				</form>
			</div>
			<div class="col-md-6">
				map here
			</div>

		</div>
	</div>
</div>

<script>
	$('#parent_submit').on('click', function(e){
		e.preventDefault();
		var user_data = $("#parentForm").serialize();
		var user_url = '<?= base_url(); ?>SchoolController/saveParent';

		$.ajax({
			type: "POST",
			url: user_url,
			dataType: 'json',
			data: user_data,
			success: function(response){

				if (response.status == 400) {
					smoke.signal(response.error, function(e){

					}, {
						duration: 2000,
						classname: ""
					});
				} else {
					smoke.signal(response.message, function(e){
					}, {
						duration: 2000,
						classname: ""
					});
					$('#parentForm')[0].reset();
				}
			}
		});
	});
</script>