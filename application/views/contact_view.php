<div class="container">
	<div class="row contact_us">
		<div class="col-md-12">
			<h1>CONTACT US</h1>
			<div class="reminder text-center">
				<p>
					FOR INQUIRIES, COMMENTS AND SUGGESTIONS, GET IN TOUCH WITH BELOW.
				</p>
				<hr>
			</div>
			<?php echo form_open("HomeController/submitContact", array('id' => 'contactForm'));?>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">NAME</label>
						<input type="text" class="form-control" name="name">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">EMAIL</label>
						<input type="text" class="form-control" name="email">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">CONTACT NUMBER</label>
						<input type="text" class="form-control" name="contact_number">
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label for="">MESSAGE</label>
						<textarea class="form-control" cols="30" rows="10" name="message"></textarea>
					</div>
					<div class="form-group">
						<button class="btn app-btn" id="contact_submit">SUBMIT</button>
					</div>
				</div>
				<div class="col-md-4">
					<div class="business_address_details">
						<h3><b>SCHOOL BUS RADAR INC.</b></h3>
						<strong>ADDRESS:</strong>
						<p>
							2/F DOLMAR BLDG. EDSA 1550
							MANDALUYONG CITY METRO MANILA,
							PHILIPPINES
						</p>
						<strong>TEL:</strong>
						<p>
							+63 726 0835<br>
							+63 917 6572 273
						</p>
						<strong>FAX:</strong>
						<p>
							+63 726 0851
						</p>
					</div>
				</div>
			<?php echo form_close();?>
		</div>
	</div>
</div>