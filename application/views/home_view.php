      <div class="jumbo">
      	<div class="dot-cover">
      		<div class="slider">
      			<div>
      				<img src="<?= base_url();?>assets/images/carousel1.jpg" alt="">
      			</div>
      			<div>
      				<img src="<?= base_url();?>assets/images/carousel1.jpg" alt="">
      			</div>
      			<div>
      				<img src="<?= base_url();?>assets/images/carousel1.jpg" alt="">
      			</div>
      		</div>
      	</div>
      	<div class="jumbo-caption">
      		<h1>SAFETY IS OUR PRIMARY PRIORITY</h1>
      		<hr>
      		<div class="col-md-12">
      			<a href="<?= base_url(); ?>about-us" class="btn jumbo-btn app-btn"><strong>READ MORE</strong></a>
      			<a href="<?= base_url(); ?>login" class="btn jumbo-btn app-btn"><strong>THE PORTAL</strong></a>
      		</div>

      	</div>
      </div>

      <div class="container welcome_message">
      	<h1>WELCOME</h1>
      	<p>
      		Welcome to the Student Tracking system for school bus Portal. This portal caters a different and unique service for our valued customers. 
      	</p>

      	<div class="row feature_section">
      		<div class="col-md-4">
      			<div class="rfid_feature feature">
      				<h1>RFID<br>TECHNOLOGY</h1>
      				<hr>
      				<p>
      					Be notified in just a tap of a card!
      				</p>
      				<a href="<?= base_url(); ?>about-us" class="pull-right">Learn More <i class="fa fa-chevron-right"></i></a>
      			</div>
      		</div>
      		<div class="col-md-4">
      			<div class="sms_feature feature">
      				<h1>SMS<br>NOTIFICATION</h1>
      				<hr>
      				<p>
      					Not an Android or iOS phone user? Not a Problem!
      				</p>
      				<a href="<?= base_url(); ?>about-us" class="pull-right">Learn More <i class="fa fa-chevron-right"></i></a>
      			</div>
      		</div>
      		<div class="col-md-4">
      			<div class="portal_feature feature">
      				<h1>PARENT<br>WEB PORTAL</h1>
      				<hr>
      				<p>
      					Avail then Log-in!
      				</p>
      				<a href="<?= base_url(); ?>about-us" class="pull-right">Learn More <i class="fa fa-chevron-right"></i></a>
      			</div>
      		</div>
      	</div> <!-- /.feature_section -->
      	<hr>

      	<!-- <div class="row news_events_section">
      		<div class="col-md-8">
      			<h1>NEWS</h1>
      			<div class="blog_slider">
      				<div><img class="img-responsive" src="<?= base_url(); ?>assets/images/firstday2.jpg" alt=""></div>
      				<div><img class="img-responsive" src="<?= base_url(); ?>assets/images/firstday2.jpg" alt=""></div>
      				<div><img class="img-responsive" src="<?= base_url(); ?>assets/images/firstday2.jpg" alt=""></div>
      			</div>
      			<div class="blog_detail_slider">
      				<h1>FIRST DAY OF SCHOOL FRILLS</h1>
      				<i class="pull-left">July 22, 2016</i>
      				<br>
      				<p>
      					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem id asperiores, nobis illo repellendus aperiam odio, dicta, pariatur ut explicabo at eos ratione!
      				</p>
      			</div>
      		</div>
      		<div class="col-md-4 event_section">
      			<h1>EVENTS</h1>
      			<hr>
      			<ul class="list-unstyled">
      				<li><h3>LOREM IPSUM DOLOR SIT</h3>
      					<i>July 22, 2016</i>
      				</li>
      				<li><h3>LOREM IPSUM DOLOR SIT</h3>
      					<i>July 22, 2016</i>
      				</li>
      				<li><h3>LOREM IPSUM DOLOR SIT</h3>
      					<i>July 22, 2016</i>
      				</li>
      				<li><h3>LOREM IPSUM DOLOR SIT</h3>
      					<i>July 22, 2016</i>
      				</li>
      			</ul>
      		</div>
      	</div>  -->
            <div class="social-media">
                  <div class="col-md-6 col-md-offset-3">
                        <i class="fa fa-facebook-official"></i> /SchooBusService | <i class="fa fa-twitter-square"></i> @schoolbusservice | <i class="fa fa-instagram"></i> @schoolbusservice
                  </div>
            </div>
      	<!-- <div class="row testimonial_section">
      		<div class="col-md-12">
      			<h1>TESTIMONIALS</h1>
      			<div class="col-md-10 col-md-offset-1">
      				<div class="testimonial_slider center">
      					<div>
      						<div class="center">
      							<img src="http://placekitten.com/100/100" alt="..." class="img-circle">
      							<h4>BRYAN WHITE, 37</h4>
      							<p>
      								<i>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam accusantium aliquam autem, perferendis vero sit delectus. Debitis ad, in dolor."</i>
      							</p>
      						</div>
      					</div>
      					<div>
      						<div class="center">
      							<img src="http://placekitten.com/100/100" alt="..." class="img-circle">
      							<h4>LILY WHILST, 44</h4>
      							<p>
      								<i>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem assumenda omnis, eveniet corporis natus aspernatur. Quidem a dolorem ipsum, repellat."</i>
      							</p>

      						</div>
      					</div>
      					<div>
      						<div class="center">
      							<img src="http://placekitten.com/100/100" alt="..." class="img-circle">
      							<h4>BERNARD ROY, 52</h4>
      							<p>
      								<i>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim repellendus, deleniti voluptate et accusamus saepe dolorum culpa quod. Voluptatem, distinctio."</i>
      							</p>
      						</div>
      					</div>
      					<div>
      						<div class="center">
      							<img src="http://placekitten.com/100/100" alt="..." class="img-circle">
      							<h4>JOHN DOE, 45</h4>
      							<p>
      								<i>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam tenetur sint corrupti aperiam provident, eaque, nostrum tempore numquam! Non, corporis."</i>
      							</p>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div> --> 
      </div> <!-- /.container -->
