<div class="container">
	<div class="row login_view">
		<div class="col-md-12">
			<h1>LOGIN</h1>
			<hr>
			<div class="row login">
				<div class="col-md-3">
					<div class="side_nav">
						<ul class="text-center nav" role="tablist">
							<li class="active"><a href="#parent" aria-controls="parent" role="tab" data-toggle="tab">PARENT</a></li>
							<li><a href="#school" aria-controls="school" role="tab" data-toggle="tab">SCHOOL</a></li>
							<li><a href="#admin" aria-controls="admin" role="tab" data-toggle="tab">ADMIN</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9 login-form">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="parent">
							<h3>PARENT LOGIN</h3>
							<div class="col-md-8 col-md-offset-2">
							<?php echo isset($message) ? $message : '';?>
								<?php echo form_open("auth/login");?>
									<div class="form-group">
										<label for="">USERNAME</label>
										<input type="text" name="identity" class="form-control">
									</div>
									<div class="form-group">
										<label for="">PASSWORD</label>
										<input type="password" name="password" class="form-control">
									</div>
									<div class="form-group">
										<button class="btn login app-btn">SUBMIT</button>
									</div>
									<div class="text-center">
										<a href="">FORGOT PASSWORD</a>
									</div>
								<?php echo form_close();?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="school">
							<h3>SCHOOL LOGIN</h3>
							<div class="col-md-8 col-md-offset-2">
							<?php echo isset($message) ? $message : '';?>
								<?php echo form_open("auth/login");?>
									<div class="form-group">
										<label for="">USERNAME</label>
										<input type="text" name="identity" class="form-control">
									</div>
									<div class="form-group">
										<label for="">PASSWORD</label>
										<input type="password" name="password" class="form-control">
									</div>
									<div class="form-group">
										<button class="btn login app-btn">SUBMIT</button>
									</div>
									<div class="text-center">
										<a href="">FORGOT PASSWORD</a>
									</div>
								<?php echo form_close();?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="admin">
							<h3>ADMIN LOGIN</h3>
							<div class="col-md-8 col-md-offset-2">
							<?php echo isset($message) ? $message : '';?>
								<?php echo form_open("auth/login");?>
									<div class="form-group">
										<label for="">USERNAME</label>
										<input type="text" name="identity" class="form-control">
									</div>
									<div class="form-group">
										<label for="">PASSWORD</label>
										<input type="password" name="password" class="form-control">
									</div>
									<div class="form-group">
										<button class="btn login app-btn">SUBMIT</button>
									</div>
									<div class="text-center">
										<a href="">FORGOT PASSWORD</a>
									</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>