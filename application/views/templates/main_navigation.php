<nav class="navbar navbar-default navbar-fixed-top">

  <div class="container-fluid">

    <div class="container">

      <!-- Brand and toggle get grouped for better mobile display -->

      <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

          <span class="sr-only">Toggle navigation</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

        </button>

        <a class="navbar-brand" href="<?= base_url(); ?>">

          <span><img width="20" alt="Brand" src="<?= base_url(); ?>assets/images/logo.png"></span>

          <h4 class="brand-title"><span class="color-yellow">SCHOOL BUS</span> RADAR</h4>

        </a>

      </div>



      <!-- Collect the nav links, forms, and other content for toggling -->

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav navbar-right">

          <li><a class="<?php echo activeUrl('', 'active-btn'); ?>" href="<?= base_url('/'); ?>">HOME</a></li>

          <li><a class="<?php echo activeUrl('about-us', 'active-btn'); ?>" href="<?= base_url('about-us'); ?>">ABOUT US</a></li>

          <li><a class="<?php echo activeUrl('contact-us', 'active-btn'); ?>" href="<?= base_url('contact-us'); ?>">CONTACT US</a></li>

          <li><a class="<?php echo activeUrl('login', 'active-btn'); ?>" href="<?= base_url('login'); ?>">LOGIN</a></li>

        </ul>

      </div><!-- /.navbar-collapse -->

    </div> <!-- /.container -->

  </div><!-- /.container-fluid -->

</nav>