<footer>
	<div class="container">
		<div class="col-md-6">
			<p>
				Copyright 2016 SchoolBusRadar Inc.
			</p>
		</div>
		<div class="col-md-6">
			<p class="pull-right">Portal | About Us | Contact Us</p>
		</div>
	</div>
</footer>
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/slick/slick.min.js"></script>
<script src="<?= base_url();?>assets/js/smoke.min.js"></script>

<script>
	$(document).ready(function() {

          $('#contact_submit').on('click', function(e){
               e.preventDefault();
               
               var contact_data = $("#contactForm").serialize();
               var url_contact = '<?= base_url(); ?>HomeController/submitContact';
               $.ajax({
                 type: "POST",
                 url: url_contact,
                 dataType: 'json',
                 data: contact_data,
                 success: function(response){
                    if (response.error) {
                    	smoke.signal(response.error, function(e){
	                    }, {
	                        duration: 2000,
	                        classname: ""
	                    });
                    } else {
                    	smoke.signal(response.message, function(e){
	                    }, {
	                        duration: 2000,
	                        classname: ""
	                    });

	                    $('#contactForm')[0].reset();
                    }
                 }
               });
          });

		// slick
		$('.slider').slick({
		lazyLoad: 'ondemand',
	    // centerMode: true,
	    // centerPadding: '60px',
	    dots: true,
	    fade: true,
	    /* Just changed this to get the bottom dots navigation */
	    infinite: true,
	    speed: 300,
	    autoplay: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    arrows: true,
	    mobileFirst: true,
	    cssEase: 'linear'
	});
		$('.blog_slider').slick({
		lazyLoad: 'ondemand',
	    // centerMode: true,
	    // centerPadding: '60px',
	    // dots: true,
	    fade: true,
	    /* Just changed this to get the bottom dots navigation */
	    infinite: true,
	    speed: 300,
	    autoplay: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    arrows: true,
	    mobileFirst: true,
	    cssEase: 'linear'
	});
		$('.testimonial_slider').slick({
		lazyLoad: 'ondemand',
	    // centerMode: true,
	    // centerPadding: '60px',
	    // dots: true,
	    // fade: true,
	    /* Just changed this to get the bottom dots navigation */
	    infinite: true,
	    slidesToShow: 3,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 4000,
	    arrows: true,
	    mobileFirst: true,
	    cssEase: 'linear'
	});
	});
</script>
</body>
</html>