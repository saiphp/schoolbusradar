<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <title>School Bus Radar</title>

  <!-- Google Fonts -->

  <link href='https://fonts.googleapis.com/css?family=Raleway:400,300' rel='stylesheet' type='text/css'>

  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>

  <!-- fontawesome -->

  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-awesome.min.css">

  <!-- Bootstrap -->

  <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

  <!-- Slick -->

  <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/slick/slick.css" />

  <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/slick/slick-theme.css"/>

  <link rel="stylesheet" href="<?= base_url();?>assets/css/smoke.css"/>

  <link href="<?= base_url(); ?>assets/css/app2.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

      <![endif]-->

    </head>

    <body>





