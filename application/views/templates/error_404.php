<style>
	.error_container {
		height: 76vh;
		padding-top: 20px;
	}

	.error_container h1 {
		font-size: 50px;
	}
</style>
<div class="container">
	<div class="col-md-12">
		<div class="text-center error_container">
			<h1>404 page not found!</h1>
			<p><i>"I can do all things through Christ who strengthens me."</i><small>- Philippians 4:13</small></p>
		</div>
	</div>
</div>