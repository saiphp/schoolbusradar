<footer>
	<div class="container">
		<div class="col-md-6">
			<p>
				Copyright 2016 SchoolBusRadar Inc.
			</p>
		</div>
		<div class="col-md-6">
			<p class="pull-right">Portal | About Us | Contact Us</p>
		</div>
	</div>
</footer>
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/slick/slick.min.js"></script>
<script src="<?= base_url();?>assets/js/smoke.min.js"></script>
<script src="<?= base_url();?>assets/js/app3.js"></script>

<script>
	$(document).ready(function() {
	      

        $('#user_submit').on('click', function(e){
		e.preventDefault();
		var user_data = $("#userForm").serialize();
		var user_url = '<?= base_url(); ?>SchoolController/saveUsers';

		$.ajax({
			type: "POST",
			url: user_url,
			dataType: 'json',
			data: user_data,
			success: function(response){

				if (response.status == 400) {
					smoke.signal(response.error, function(e){

					}, {
						duration: 2000,
						classname: ""
					});
				} else {
					smoke.signal(response.message, function(e){
					}, {
						duration: 2000,
						classname: ""
					});
					$('#userForm')[0].reset();
				}
			}
		});
	});

	$('#school_submit').on('click', function(e){
		e.preventDefault();
		var user_data = $("#schoolForm").serialize();
		var user_url = '<?= base_url(); ?>SchoolController/saveSchool';

		$.ajax({
			type: "POST",
			url: user_url,
			dataType: 'json',
			data: user_data,
			success: function(response){
				if (response.status == 400) {
					smoke.signal(response.error, function(e){
					}, {
						duration: 2000,
						classname: ""
					});
				} else {
					smoke.signal(response.message, function(e){
					}, {
						duration: 2000,
						classname: ""
					});
					$('#schoolForm')[0].reset();
				}
			}
		});
	});

	// dataTable
	$('#school_table').DataTable({
		ajax: {
			url: '<?= base_url(); ?>SchoolController',
			dataSrc: ''
		},
		columns: [
		{ data: "id" },
		{ data: "name" },
		{ data: "description" },
		]
	});

	$('#user_table').DataTable({
		ajax: {
			url: '<?= base_url(); ?>SchoolController/users',
			dataSrc: ''
		},
		columns: [
		{ data: "id" },
		{ data: "username" },
		{ data: "first_name" },
		{ data: "last_name" },
		{ data: "company" },
		]
	});

	$('#inquiry_table').DataTable({
		ajax: {
			url: '<?= base_url(); ?>SchoolController/inquiry',
			dataSrc: ''
		},
		columns: [
		{ data: "id" },
		{ data: "name" },
		{ data: "email" },
		{ data: "contact_number" },
		{ data: "message" },
		]
	});

           
          $('.dropdown-toggle').dropdown();
	});
</script>
</body>
</html>