<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class MY_Controller extends CI_Controller {



	function __construct()

	{

		parent::__construct();

		$this->load->helper('app_helper');

		$this->load->library('form_validation');

	}

	

	public function site_template($view, $data = '')

	{

		$this->load->view('templates/header');

		$this->load->view('templates/main_navigation');

		$this->load->view($view, $data);

		$this->load->view('templates/footer');

	}



	public function admin_template($view, $data = '')

	{

		$this->load->view('templates/dashboard_header');

		$this->load->view('templates/dashboard_navigation');

		$this->load->view($view, $data);

		$this->load->view('templates/admin_footer');

	}



	public function school_template($view, $data = '')

	{

		$this->load->view('templates/dashboard_header');

		$this->load->view('templates/school_navigation');

		$this->load->view($view, $data);

		$this->load->view('templates/footer');

	}





}