<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('activeUrl'))
{
	function activeUrl($uri, $active)
	{
		$CI =& get_instance();
		if ($uri == $CI->uri->segment(1) ) {
			return $active;
		} else {
			return '';
		}
	}
}
